from django.shortcuts import render
from .models import GroceryItem

# Create your views here.


def index(request):
    grocery_items = GroceryItem.objects.all()
    return render(request, 'django_practice/index.html', {'grocery_items': grocery_items})